const { initialize } = require('zokrates-js')
const fs = require("fs");

//let a = 3;

/*
Here I have to define the inputs received from rust
*/

let root = ["0x5e491e9c", "0x1aa576c8", "0xada12ad5", "0x66ba0388", "0x3a120042", "0x7680ae6d", "0xe2fdac92", "0x6cd72683"];
let leaf = ["3", "3", "3", "3", "3", "3", "3", "3"];
let direction_selector = ["1", "0"];
let path = [["4", "4", "4", "4", "4", "4", "4", "4"], ["8", "8", "8", "8", "8", "8", "8", "8"]];
//let a_pk_i_old = ["0xabed6794", "0x1cd0789b", "0x992b2550", "0x6f6394f4", "0xc964f8bb", "0xa9ee0e60", "0xf2680195", "0xdb5b421d"];
let a_pk_i_old = ["0x49fc0325", "0xae9924ee", "0x7b9ffb1b", "0x6a5d7f4c", "0xc00b11bc", "0x7a793061", "0x3fb0486e", "0x46300447"];
let a_sk_i_old = ["0x01234567", "0x89abcdef", "0xfedcba98", "0x76543210", "0x00112233", "0x44556677", "0x8899aabb", "0xccddeeff"];
let rho_i_old = ["3", "3", "3", "3", "3", "3", "3", "3"];
let sn_i_old = ["0x0ec7bb1f", "0x29caa79d", "0xf34fc2b3", "0xc9975f2f", "0xe708d354", "0xac20b348", "0x952dc174", "0x360eadd2"];
let cm_i_old = ["0x006cbcb7", "0x92e8d5c8", "0xb0376f8c", "0xcebcc149", "0x6e73661f", "0x06a4f921", "0x1ea3a047", "0x2525cd93"];
let r_i_old = ["0x006cbcb7", "0x92e8d5c8", "0xb0376f8c", "0xcebcc149", "0x6e73661f", "0x06a4f921", "0x1ea3a047", "0x2525cd93"];
let v_i_old = ["0x006cbcb7", "0x92e8d5c8", "0xb0376f8c", "0xcebcc149", "0x6e73661f", "0x06a4f921", "0x1ea3a047", "0x2525cd93"];

let a_pk_i_new = ["2", "2", "2", "2", "2", "2", "2", "2"];
let rho_i_new = ["2", "2", "2", "2", "2", "2", "2", "2"];
let r_i_new = ["2", "2", "2", "2", "2", "2", "2", "2"];
let v_i_new = ["2", "2", "2", "2", "2", "2", "2", "2"];
let h_i = ["2", "2", "2", "2", "2", "2", "2", "2"];
let h_sig = ["2", "2", "2", "2", "2", "2", "2", "2"];


initialize().then((zokratesProvider) => {

    //const source = "def main(private field a, field b) -> bool { bool b = a*a==b? true:false; return b; }";

    const source = fs.readFileSync("./zerocash.zok").toString();

    // compilation
    const artifacts = zokratesProvider.compile(source);
    console.log("artifacts: "+artifacts);

    // computation
    const { witness, output } = zokratesProvider.computeWitness(
        artifacts, 
        [root, leaf, direction_selector, path, a_pk_i_old, a_sk_i_old, rho_i_old, 
            sn_i_old, cm_i_old, r_i_old, v_i_old, a_pk_i_new, rho_i_new, r_i_new, v_i_new,
            h_i, h_sig]
        );
    
    //console.log("witness: " + witness);
    console.log("output: " + output);

    /*
    // computation
    const { witness, output } = zokratesProvider.computeWitness(artifacts, ["2", "4"]);
    console.log("witness: " + witness);
    console.log("output: " + output);

    // run setup
    const keypair = zokratesProvider.setup(artifacts.program);
    console.log("keypair: " + keypair);

    // generate proof
    const proof = zokratesProvider.generateProof(artifacts.program, witness, keypair.pk);
    console.log("proof: " + proof);

    // export solidity verifier
    const verifier = zokratesProvider.exportSolidityVerifier(keypair.vk);
    console.log("verifier: " + verifier);
    
    // or verify off-chain
    const isVerified = zokratesProvider.verify(keypair.vk, proof);
    console.log("isVerified: " + isVerified);

    //console.log(a);
    */
});
