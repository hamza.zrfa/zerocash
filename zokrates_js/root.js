const { initialize } = require('zokrates-js')
const fs = require("fs");

//let a = 3;

/*
Here I have to define the inputs received from rust
*/

initialize().then((zokratesProvider) => {
    //const source = "def main(private field a, field b) -> bool { bool b = a*a==b? true:false; return b; }";

    const source = fs.readFileSync("./test.zok").toString();

    // compilation
    const artifacts = zokratesProvider.compile(source);
    console.log("artifacts: "+artifacts);

    // computation
    const { witness, output } = zokratesProvider.computeWitness(artifacts, ["2", "4"]);
    console.log("witness: " + witness);
    console.log("output: " + output);

    // run setup
    const keypair = zokratesProvider.setup(artifacts.program);
    console.log("keypair: " + keypair);

    // generate proof
    const proof = zokratesProvider.generateProof(artifacts.program, witness, keypair.pk);
    console.log("proof: " + proof);

    // export solidity verifier
    const verifier = zokratesProvider.exportSolidityVerifier(keypair.vk);
    console.log("verifier: " + verifier);
    
    // or verify off-chain
    const isVerified = zokratesProvider.verify(keypair.vk, proof);
    console.log("isVerified: " + isVerified);

    //console.log(a);
});
