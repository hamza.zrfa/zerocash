use sha2::{Sha256, Digest};

pub fn getSha256Hash(data: impl AsRef<[u8]>) -> String{
    let mut hasher = Sha256::new();
    hasher.update(data);
    let hash = hasher.finalize();
    format!("{:x}", hash)
}