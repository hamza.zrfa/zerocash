extern crate secp256k1;
use secp256k1::{ ecdsa, Message, PublicKey, Secp256k1, SecretKey, KeyPair};

pub fn sign(message: &[u8], secret_key: &SecretKey) -> ecdsa::Signature{
    let secp = Secp256k1::new();
    secp.sign_ecdsa(&Message::from_slice(message).expect("32 bytes"), secret_key)
}

pub fn verify(message: &[u8; 32], sig : &ecdsa::Signature, public_key: &PublicKey) -> bool{
    let secp = Secp256k1::new();
    //use Secp256k1::verification_only() instead
    secp.verify_ecdsa(&Message::from_slice(message).expect("32 bytes"), sig, public_key).is_ok()
}

pub fn generate_keypair() -> (PublicKey, SecretKey){

    let secp = Secp256k1::new();
    let mut rng = rand::thread_rng();

    let key_pair = KeyPair::new(&secp, &mut rand::thread_rng());

    let secret_key = key_pair.secret_key();
    let public_key = key_pair.public_key();

    (public_key, secret_key)
}