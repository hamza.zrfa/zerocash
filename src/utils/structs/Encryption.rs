use rsa::{RsaPublicKey, RsaPrivateKey};

pub struct Encryption{

}

impl Encryption{
    pub fn get_public_address_encryption(private_address: RsaPrivateKey) -> RsaPublicKey{
        RsaPublicKey::from(&(private_address))
    }

    pub fn generate_private_address_encryption() -> RsaPrivateKey{
        let mut rng = rand::thread_rng();
        let bits = 2048;
        RsaPrivateKey::new(&mut rng, bits).expect("failed to generate a key")
    }
}