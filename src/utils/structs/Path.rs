extern crate serde;
use std::default;

use serde::Serialize;

#[repr(C)]
#[derive(Default)]
pub struct Path{
    indices_to_prove: Vec<usize>, 
    leaves_to_prove: [[u8; 32]; 32],
}

impl Path{
    pub fn new(indices_to_prove: Vec<usize>, leaves_to_prove: [[u8; 32]; 32]) -> Self{Path{
        indices_to_prove,
        leaves_to_prove
    }}

    pub fn getIndicesToProve(&self) -> Vec<usize>{
        self.indices_to_prove.clone()
    }

    pub fn getLeavesToProve(&self) -> [[u8; 32]; 32]{
        self.leaves_to_prove
    }
}