extern crate serde;
extern crate num_bigint;
extern crate hex;

use hex::decode;
use num_bigint::BigUint;

use serde::Serialize;
use crate::hash_functions;
use super::Address::PublicAddress;
use super::Address::PrivateAddress;


#[repr(C)]
#[derive(Default)]
#[derive(Clone)]
pub struct Coin{
    addr_pk: PublicAddress,
    v: u32,
    rho: u32,
    r: u32,
    s: u32,
    cm: String,
}

impl Coin{
    pub fn new(addr_pk: PublicAddress, v: u32, rho: u32, r: u32, s: u32, cm: String ) -> Coin{
        Coin {
            addr_pk: addr_pk, 
            v: v,
            rho: rho,
            r: r,
            s: s,
            cm: cm,
        }
    }

    pub fn computeCommitmentsCm(v: u32,  k: String, s: u32) -> String{
        
        let k_value_big_uint = hexa_string_to_big_uint(k);

        let v_big_uint = BigUint::from_slice(&[v as u32]);

        let s_big_uint = BigUint::from_slice(&[s as u32]);

        //We will commit to the sum of these three integers//hash_functions::getSha256Hash((((v as u128) << 128) | ((k_value as u128) << 64) | (s as u128)).to_le_bytes())
        hash_functions::getSha256Hash((v + k_value_big_uint + s).to_bytes_le())
    }

    pub fn computeCommitmentsK(a_pk: u32,  rho: u32, r: u32) -> String{
        let a_pk_big_uint = BigUint::from_slice(&[a_pk as u32]);
        
        let rho_big_uint = BigUint::from_slice(&[rho as u32]);

        let r_big_uint = BigUint::from_slice(&[r as u32]);

        //We will commit to the sum of these three integers//hash_functions::getSha256Hash((((v as u128) << 128) | ((k_value as u128) << 64) | (s as u128)).to_le_bytes())
        hash_functions::getSha256Hash((a_pk_big_uint + rho_big_uint + r_big_uint).to_bytes_le())
    }

    pub fn getCoinCommitment(&self) -> String {
        self.cm.clone()
    }

    pub fn getSpendingNumber(&self) -> u32 {
        self.s
    }

    pub fn getRho(&self) -> u32{
        self.rho
    }

    pub fn getR(&self) -> u32{
        self.r
    }

    pub fn getValue(&self) -> u32{
        self.v
    }
    
    pub fn getPublicAddress(&self) -> PublicAddress{
        self.addr_pk.clone()
    }

    pub fn getTxMint(&self) -> (String, u32, String, u32){
        //let k = hash_functions::getSha256Hash((((self.addr_pk.a_pk as u128) << 128) | ((self.rho as u128) << 64) | (self.r as u128)).to_le_bytes());
        let k = hash_functions::getSha256Hash((self.addr_pk.a_pk + self.rho + self.r).to_le_bytes());
        (self.getCoinCommitment(), self.v, k, self.s)
    }

}

pub fn hexa_string_to_big_uint(hex_str: String) -> num_bigint::BigUint{
    // Convert the hexadecimal string to bytes
    let bytes = hex::decode(hex_str).unwrap();

    // Convert the bytes to a BigUint
    BigUint::from_bytes_be(&bytes)
}