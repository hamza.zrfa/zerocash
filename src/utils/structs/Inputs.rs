extern crate serde;
use serde::Serialize;

use crate::utils::structs::Path::Path;
use crate::utils::structs::Coin::Coin;
use crate::utils::structs::Address::PrivateAddress;



#[repr(C)]
#[derive(Default)]
#[derive(Clone)]
pub struct X{
    rt: [u8; 32],
    sn_1_old: String,
    sn_2_old: String,
    cm_1_new: String,
    cm_2_new: String,
    v_pub: u32,
    h_sig: String,
    h_1: u32,
    h_2: u32
}

impl X{
    pub fn new(rt: [u8; 32],
        sn_1_old: String,
        sn_2_old: String,
        cm_1_new: String,
        cm_2_new: String,
        v_pub: u32,
        h_sig: String,
        h_1: u32,
        h_2: u32) -> Self{
            X{
                rt,
                sn_1_old,
                sn_2_old,
                cm_1_new,
                cm_2_new,
                v_pub,
                h_sig,
                h_1,
                h_2
            }
        }
}


#[repr(C)]
#[derive(Default)]
pub struct A{
    path_1: Path,
    path_2: Path,
    c_1_old: Coin,
    c_2_old: Coin,
    addr_sk_1_old: PrivateAddress,
    addr_sk_2_old: PrivateAddress,
    c_1_new: Coin,
    c_2_new: Coin
}

impl A{
    pub fn new(
        path_1: Path,
        path_2: Path,
        c_1_old: Coin,
        c_2_old: Coin,
        addr_sk_1_old: PrivateAddress,
        addr_sk_2_old: PrivateAddress,
        c_1_new: Coin,
        c_2_new: Coin
    ) -> Self{
        A{
            path_1,
            path_2,
            c_1_old,
            c_2_old,
            addr_sk_1_old,
            addr_sk_2_old,
            c_1_new,
            c_2_new,
        }
    }
}