use rand::{Rng};
use super::{Coin::Coin, Address::{PublicAddress}};
use super::super::traits::transaction::Transaction;

pub struct TxMint{
    cm: String,
    v: u32,
    k: String,
    s: u32,
}

impl Transaction for TxMint{
    fn verify(&self)-> bool {
        //Set cm_:=COMM_s(v||k)
        let cm_ = Coin::computeCommitmentsCm(self.v, self.k.clone(), self.s);

        //Output 1 if cm==cm_, else output false
        cm_==self.cm
    }
}

impl TxMint{
    pub fn new(cm: String, v: u32, k: String, s: u32) -> TxMint{
        TxMint{
            cm: cm,
            v: v,
            k: k,
            s: s, 
        }
    }

    pub fn mint(_pp: String, v: u32, addr_pk: PublicAddress) -> (Coin, TxMint){

        //Randomly sample a PRF-sn seed rho
        let mut rho: u32 = rand::thread_rng().gen();

        //Randomly sample two COMM trapdoors r, s
        let mut r: u32 = rand::thread_rng().gen();
        let mut s: u32 = rand::thread_rng().gen();

        //Compute k:=COMM-r(a_pk||rho):=COMM(a_pk||rho||r)
        let k = Coin::computeCommitmentsK(addr_pk.a_pk, rho, r);

        //Compute cm:=COMM-r(v||k):=COMM(v||k||s)
        let cm = Coin::computeCommitmentsCm(v, k.clone(), s);

        //Set c:=(addr_pk, v, rho, r, s, cm)
        let c = Coin::new(addr_pk, v, rho, r, s, cm.clone());

        //Set tx_mint:=(cm, v, k, s)
        let txMint = TxMint::new(cm.clone(), v, k.clone(), s);

        //Output c and tx_mint
        (c, txMint)
    }

}