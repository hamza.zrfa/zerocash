use std::mem;
use rand::{rngs::StdRng, thread_rng, SeedableRng, Rng};
use num_bigint::BigUint;
use rsa::{PublicKey, PaddingScheme, RsaPublicKey, RsaPrivateKey};
use secp256k1::{Secp256k1, ecdsa};

use crate::{utils::{signature_fonctions::{generate_keypair, sign, verify}, hash_functions}};
use super::{Address::{PrivateAddress, PublicAddress}, Ledger::Ledger, MerkleTree::MerkleTree};
use super::Coin::Coin;
use super::Inputs::{A, X};
use super::Path::Path;
use crate::utils::traits::transaction::Transaction;

//use crate::utils::

pub struct TxPour{
    rt: [u8; 32],
    sn_1_old: String,
    sn_2_old: String,
    cm_1_new: String,
    cm_2_new: String,
    v_pub: u32,
    info: String,
    pk_sig: secp256k1::PublicKey,
    h_1: u32,
    h_2: u32,
    pi_pour: String,
    C_1: Vec<u8>,
    C_2: Vec<u8>,
    sigma: ecdsa::Signature,
}

impl Transaction for TxPour{
    fn verify(&self)-> bool {
        //Get the ledger
            //The Ledger has not been implemented yet, todo!
        //let L : Ledger = Ledger{};

        //If sn_1-old or sn_2-old appears on L (or sn_1-old==sn_2-old), output false
        if Ledger::containsSn(self.sn_1_old.clone())||Ledger::containsSn(self.sn_2_old.clone())||(self.sn_1_old==self.sn_2_old){
            return false;
        }

        //If the Merkle root rt does not appears on L, output false

            //Construction of the Merkle Tree
        let merkle_tree = MerkleTree::getMerkleTree();
        let rt = MerkleTree::getMerkleRoot(merkle_tree);
        Ledger::containsMerkleRoot(rt);

        //Computation of h_sig:=CRH(pk_sig)
        let h_sig = hash_functions::getSha256Hash(self.pk_sig.to_string().as_bytes());

        //Set x:=(rt, sn_1-old, sn_2-old, cm_1-new, cm_2-new, v_pub, h_sig, h_1, h_2)
        let x = X::new(
            rt,
            self.sn_1_old.clone(),
            self.sn_2_old.clone(),
            self.cm_1_new.clone(),
            self.cm_2_new.clone(),
            self.v_pub,
            h_sig,
            self.h_1,
            self.h_2
        );

        //Set m:=(x, pi_pour, info, C_1, C_2)
        let m=(x, self.pi_pour.clone(), self.info.clone(), self.C_1.clone(), self.C_2.clone());

        //Computation of b:=V_sig(pk_sig, m, sigma)
        let b = verify_tuple(m, &self.pk_sig, &self.sigma);

        //Computation of b_:=Verify(vk_pour, x, pi_pour); todo!
        let b_ = true;

        b&&b_
    }
}

impl TxPour{
    pub fn new(
        rt: [u8; 32],
        sn_1_old: String,
        sn_2_old: String,
        cm_1_new: String,
        cm_2_new: String,
        v_pub: u32,
        info: String,
        pk_sig: secp256k1::PublicKey,
        h_1: u32,
        h_2: u32,
        pi_pour: String,
        C_1: Vec<u8>,
        C_2: Vec<u8>,
        sigma: ecdsa::Signature) -> TxPour {
        TxPour{
            rt,
            sn_1_old,
            sn_2_old,
            cm_1_new,
            cm_2_new,
            v_pub,
            info,
            pk_sig,
            h_1,
            h_2,
            pi_pour,
            C_1,
            C_2,
            sigma,
        }
    }

    pub fn pour(
            _pp: u32,
            rt: [u8; 32], 
            c_1_old: Coin, 
            c_2_old: Coin, 
            addr_sk_1_old: PrivateAddress,
            addr_sk_2_old: PrivateAddress,
            path_1: Path,
            path_2: Path,
            v_1_new: u32,
            v_2_new: u32,
            addr_pk_1_new: PublicAddress,
            addr_pk_2_new: PublicAddress,
            v_pub: u32,
            info: String
        ) -> (Coin, Coin, TxPour){

            //Compute sn_i-old:=PRF-sn_{a_{sk,i}-old}(rho_i-old)
            //let seed_1 = ((c_1_old.getRho() as u32) << 16) | (addr_sk_1_old.a_sk as u32);
            //let mut PRF_1: StdRng = SeedableRng::from_seed(u128_to_bytes(seed_1));

            //let seed_2 = ((c_1_old.getRho() as u32) << 16) | (addr_sk_1_old.a_sk as u32);
            //let mut PRF_2: StdRng = SeedableRng::from_seed(u128_to_bytes(seed_2));

            /*
            let sn_1_old = PRF_1.gen();
            let sn_2_old = PRF_2.gen();
            */
            
            /*Step 1*/

            let c_1_old_rho_big_uint = BigUint::from_slice(&[c_1_old.getRho()]);
            let addr_sk_1_old_a_sk_big_uint = BigUint::from_slice(&[addr_sk_1_old.a_sk]);
        
            let c_2_old_rho_big_uint = BigUint::from_slice(&[c_2_old.getRho()]);
            let addr_sk_2_old_a_sk_big_uint = BigUint::from_slice(&[addr_sk_2_old.a_sk]);

                //We will commit to the sum of these two integers
            let sn_1_old = hash_functions::getSha256Hash((c_1_old_rho_big_uint + addr_sk_1_old_a_sk_big_uint).to_bytes_le());
            let sn_2_old = hash_functions::getSha256Hash((c_2_old_rho_big_uint + addr_sk_2_old_a_sk_big_uint).to_bytes_le());

            //Randomly sample a PRF-sn seed rho_i-new
            let rho_1_new: u32 = rand::thread_rng().gen();
            let rho_2_new: u32 = rand::thread_rng().gen();

            //Randomly sample two COMM trapdoors r_i-new, s_i-new
            let r_1_new: u32 = rand::thread_rng().gen();
            let r_2_new: u32 = rand::thread_rng().gen();

            let s_1_new: u32 = rand::thread_rng().gen();
            let s_2_new: u32 = rand::thread_rng().gen();

            //Compute k_i-new:=COMM_{r_i-new}(a_{pk,i}-new||rho_i-new)
            let k_1_new = Coin::computeCommitmentsK(addr_pk_1_new.a_pk.clone(), rho_1_new, r_1_new);
            let k_2_new = Coin::computeCommitmentsK(addr_pk_2_new.a_pk.clone(), rho_2_new, r_2_new);

            //Compute cm_i_new:=COMM_{s_i-new}(v_i-new||k_i-new)
            let cm_1_new = Coin::computeCommitmentsCm(v_1_new, k_1_new, s_1_new);
            let cm_2_new = Coin::computeCommitmentsCm(v_2_new, k_2_new, s_2_new);

            //Set c_i_new:=(addr_{pk,i}-new, v_i-new, rho_i-new, r_i-new, s_i-new)
            let c_1_new = Coin::new(addr_pk_1_new.clone(), v_1_new, rho_1_new, r_1_new, s_1_new, cm_1_new.clone());
            let c_2_new = Coin::new(addr_pk_2_new.clone(), v_2_new, rho_2_new, r_2_new, s_2_new, cm_2_new.clone());

            //Set C_i:=Enc(pk_{enc,i}_new, (v_i-new, rho_i-new, r_i-new, s_i-new))
            let data_1 = (v_1_new, rho_1_new, r_1_new, s_1_new);
            let C_1 = encrypt_data_coin(data_1, addr_pk_1_new.pk_enc);
            
            let data_2 = (v_2_new, rho_2_new, r_2_new, s_2_new);
            let C_2 = encrypt_data_coin(data_2, addr_pk_2_new.pk_enc);

            /*Step 2*/

            //Generate (pk_sig, sk_sig):=K_sig(pp_sig)
            
                //I'm going to use another way for the moment ; todo!
            
            let (pk_sig, sk_sig) = generate_keypair();

            /*Step 3*/

            //Compute h_sig:=CRH(pk_sig)

            let h_sig = hash_functions::getSha256Hash(pk_sig.to_string().as_bytes());

            /*Step 4*/

            //Compute h_1:=PRF_a_{sk_1}-old-pk(h_sig) and h_2:=PRF_a_{sk_2}-old-pk(h_sig)
            let h_sig_value_big_uint = hexa_string_to_big_uint(h_sig.clone());
            
            let addr_sk_1_old_a_sk_big_uint = BigUint::from_slice(&[addr_sk_1_old.a_sk]);
            let addr_sk_2_old_a_sk_big_uint = BigUint::from_slice(&[addr_sk_2_old.a_sk]);

            let seed_1: BigUint = (h_sig_value_big_uint.clone() << 64) | (addr_sk_1_old_a_sk_big_uint);
            let seed_1_as_vec_u8 : Vec<u8> = seed_1.to_bytes_le();
            let seed_1_as_fixed_size_u8_40 : [u8; 40] = seed_1_as_vec_u8.try_into().unwrap();
                //We have to convert [u8; 40] to [u8; 32] for using Seedable::from_seed
                //method
            let seed_1_as_fixed_size_u8_32 : [u8; 32] = shuffle_bits(seed_1_as_fixed_size_u8_40);
            let mut PRF_h1: StdRng = SeedableRng::from_seed(seed_1_as_fixed_size_u8_32);

            let seed_2: BigUint = (h_sig_value_big_uint.clone() << 64) | (addr_sk_2_old_a_sk_big_uint);
            let seed_2_as_vec_u8 : Vec<u8> = seed_2.to_bytes_le();
            let seed_2_as_fixed_size_u8_40 : [u8; 40] = seed_2_as_vec_u8.try_into().unwrap();
                //We have to convert [u8; 40] to [u8; 32] for using Seedable::from_seed
                //method
            let seed_2_as_fixed_size_u8_32 : [u8; 32] = shuffle_bits(seed_2_as_fixed_size_u8_40);
            let mut PRF_h2: StdRng = SeedableRng::from_seed(seed_2_as_fixed_size_u8_32);

            let h_1 = PRF_h1.gen();
            let h_2 = PRF_h2.gen();
        
            /*Step 5*/

            //Set x:=(rt, sn_1-old, sn_2-old, cm_1-new, cm_2-new, v_pub, h_sig, h_1, h_2)

            let x = X::new(
                rt,
                sn_1_old.clone(),
                sn_2_old.clone(),
                cm_1_new.clone(),
                cm_2_new.clone(),
                v_pub,
                h_sig,
                h_1,
                h_2
            );

            /*Step 6*/

            //Set a:=(path_1, path_2, c_1-old, c_2-old, addr_{sk,1}_old, addr_{sk,2}_old, c_1-new, c_2-new)

            let _a = A::new(
                path_1,
                path_2,
                c_1_old,
                c_2_old,
                addr_sk_1_old,
                addr_sk_2_old,
                c_1_new.clone(),
                c_2_new.clone(),
            );

            /*Step 7*/

            //Compute pi_pour:=Prove(pk_pour, x, a)

                //todo!

            let pi_pour = String::from("0");

            /*Step 8*/

            //Set m:=(x, pi_pour, info, C_1, C_2)

            let m = (x.clone(), pi_pour.clone(), info.clone(), C_1.clone(), C_2.clone());

            /*Step 9*/

            //Compute sigma:=S_sig(sk_sig, m)

            let sigma = sign_tuple(m, &sk_sig);

            /*Step 10*/

            //Set tx_pour:=(rt, sn_1-old, sn_2-old, cm_1-new, cm_2-new, v_pub, info, pk_sig, h_1, h_2, pi_pour, C_1, C_2, sigma)

            let tx_pour = TxPour{
                rt,
                sn_1_old: sn_1_old.clone(),
                sn_2_old: sn_2_old.clone(),
                cm_1_new: cm_1_new.clone(),
                cm_2_new: cm_2_new.clone(),
                v_pub,
                info,
                pk_sig,
                h_1,
                h_2,
                pi_pour,
                C_1,
                C_2,
                sigma
            };

            /*Step 11*/

            //Output c_1-new, c_2-new and tx_pour

            (c_1_new, c_2_new, tx_pour)
        
    }

    pub fn getC1(&self)->Vec<u8>{self.C_1.clone()}
    pub fn getC2(&self)->Vec<u8>{self.C_2.clone()}
    pub fn getCm1New(&self)->String{self.cm_1_new.clone()}
    pub fn getCm2New(&self)->String{self.cm_2_new.clone()}

}

fn encrypt_data_coin(data: (u32, u32, u32, u32), pk_enc: RsaPublicKey) -> Vec<u8>{
    let data_as_slice = tuple_to_bytes(data);
    let mut rng = rand::thread_rng();
    pk_enc.encrypt(&mut rng, PaddingScheme::new_pkcs1v15_encrypt(), &data_as_slice[..]).expect("failed to encrypt")
}

fn decrypte_data_coin(C: Vec<u8>, sk_enc: RsaPrivateKey) -> (u32, u32, u32, u32) {
    let padding = PaddingScheme::new_pkcs1v15_encrypt();
    let data_as_slice : [u8; 16] = sk_enc.decrypt(padding, &C).expect("failed to decrypt").try_into().unwrap();
    bytes_to_tuple(data_as_slice)
}

fn u128_to_bytes(value: u128) -> [u8; 32] {
    let mut arr = [0u8; 32];
    for (i, b) in value.to_le_bytes().iter().enumerate() {
        arr[i] = *b;
    }
    arr
}

fn bytes_to_u128(arr: [u8; 32]) -> u128 {
    let mut value = 0u128;
    for (i, b) in arr.iter().enumerate() {
        value |= (*b as u128) << (i * 8);
    }
    value
}

fn tuple_to_bytes(input: (u32, u32, u32, u32)) -> [u8; 16] {
    let (a, b, c, d) = input;
    let mut output = [0u8; 16];
    output[0..4].copy_from_slice(&a.to_le_bytes());
    output[4..8].copy_from_slice(&b.to_le_bytes());
    output[8..12].copy_from_slice(&c.to_le_bytes());
    output[12..16].copy_from_slice(&d.to_le_bytes());
    output
}

fn bytes_to_tuple(input: [u8; 16]) -> (u32, u32, u32, u32) {
    let a = u32::from_le_bytes(input[0..4].try_into().unwrap());
    let b = u32::from_le_bytes(input[4..8].try_into().unwrap());
    let c = u32::from_le_bytes(input[8..12].try_into().unwrap());
    let d = u32::from_le_bytes(input[12..16].try_into().unwrap());
    (a, b, c, d)
}


fn sign_tuple(m: (X, String, String, Vec<u8>, Vec<u8>), sk_sig: &secp256k1::SecretKey) -> ecdsa::Signature {
    // Convert the tuple into a slice of bytes
    let m_0_bytes = &struct_X_to_bytes(m.0);
    let m_1_bytes = m.1.as_bytes();
    let m_2_bytes = m.2.as_bytes();
    let m_3_bytes = &m.3;
    let m_4_bytes = &m.4;

    //Compute hash of the message
    let message : &[u32]= &(convert_u8_to_u32([m_0_bytes, m_1_bytes, m_2_bytes, m_3_bytes, m_4_bytes].concat()));
    let message_big_uint = BigUint::from_slice(&message);
    let message_big_uint_bytes : [u8; 32] = (message_big_uint).to_bytes_le()[..32].to_vec()[..].try_into().unwrap();
    let message_hased_hexa_string = hash_functions::getSha256Hash(message_big_uint_bytes);
    let message_hased_value : &[u8] = &hex::decode(&message_hased_hexa_string).unwrap();

    // Sign the message using the secret key
    let signature = sign(message_hased_value, sk_sig);

    //Output the signature
    signature
}

fn verify_tuple(m: (X, String, String, Vec<u8>, Vec<u8>), pk: &secp256k1::PublicKey, sig: &ecdsa::Signature) -> bool {
    // Convert the tuple into a slice of bytes
    let m_0_bytes = &struct_X_to_bytes(m.0);
    let m_1_bytes = m.1.as_bytes();
    let m_2_bytes = m.2.as_bytes();
    let m_3_bytes = &m.3;
    let m_4_bytes = &m.4;
    let message = [m_0_bytes, m_1_bytes, m_2_bytes, m_3_bytes, m_4_bytes].concat();

    // Verify the signature using the public key and message
    let context = Secp256k1::verification_only();
    verify(&message.try_into().unwrap(), sig, pk)
}

fn struct_X_to_bytes(x: X) -> Vec<u8> {
    const size: usize = mem::size_of::<X>();
    let mut bytes = Vec::with_capacity(size);
    unsafe {
        let ptr = &x as *const X;
        let arr: &[u8; size] = mem::transmute(ptr);
        bytes.extend_from_slice(arr);
    }
    bytes
}

fn bytes_to_struct_X(bytes: &[u8], size_of_type_bytes: usize) -> X
    where
        X: Default,
        X: Sized{
    assert_eq!(bytes.len(), mem::size_of::<X>());
    const size: usize = mem::size_of::<X>();
    let mut s = X::default();
    unsafe {
        let ptr = &mut s as *mut X;
        let arr: &mut [u8; size] = mem::transmute(ptr);
        arr.copy_from_slice(bytes);
    }
    s
}

pub fn hexa_string_to_big_uint(hex_str: String) -> num_bigint::BigUint{
    // Convert the hexadecimal string to bytes
    let bytes = hex::decode(hex_str).unwrap();

    // Convert the bytes to a BigUint
    BigUint::from_bytes_be(&bytes)
}

fn shuffle_bits(input: [u8; 40]) -> [u8; 32] {
    let mut output = [0u8; 32];
    let mut rng = thread_rng();

    for i in 0..32 {
        output[i] = input[rng.gen_range(0..40)];
    }

    output
}

fn convert_u8_to_u32(input: Vec<u8>) -> Vec<u32> {
    input
        .chunks_exact(4)
        .map(|chunk| {
            let mut bytes = [0; 4];
            bytes.copy_from_slice(chunk);
            u32::from_le_bytes(bytes)
        })
        .collect()
}