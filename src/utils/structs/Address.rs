extern crate serde;
use serde::Serialize;
use rsa::{RsaPublicKey, RsaPrivateKey};

#[derive(Clone)]
pub struct PublicAddress{
    pub a_pk : u32,
    pub pk_enc : RsaPublicKey, //it's a RsaPublicKey type embeded into a String
}

impl PublicAddress{
    pub fn new(a_pk : u32, pk_enc : RsaPublicKey) -> Self{
        Self{
            a_pk,
            pk_enc
        }
    }
}

impl Default for PublicAddress {
    fn default() -> Self {
        let mut rng = rand::thread_rng();
        let bits = 2048;
        let priv_key = RsaPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
        let pub_key = RsaPublicKey::from(&priv_key);
        PublicAddress {
            // specify default values for other fields of MyStruct
            a_pk : 0,
            pk_enc : pub_key,
        }
    }
}

#[repr(C)]
#[derive(Clone)]
pub struct PrivateAddress{
    pub a_sk : u32,
    pub sk_enc : RsaPrivateKey,
}

impl PrivateAddress{
    pub fn new(a_sk : u32, sk_enc : RsaPrivateKey) -> Self{
        Self{
            a_sk,
            sk_enc
        }
    }
}

impl Default for PrivateAddress {
    fn default() -> Self {
        let mut rng = rand::thread_rng();
        let bits = 2048;
        let priv_key = RsaPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
        PrivateAddress {
            // specify default values for other fields of MyStruct
            a_sk : 0,
            sk_enc : priv_key,
        }
    }
}