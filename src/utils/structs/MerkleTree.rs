extern crate rs_merkle;
use ::rs_merkle::MerkleTree as RsMerkleTree;
use rs_merkle::{*, algorithms::Sha256};

use super::Path::Path;

pub struct MerkleTree{
    //leaves_hashed_sha256: Vec<[u8; 32]>,
    //merkle_root : [u8; 32],
    //merkle_tree : rs_merkle::RsMerkleTree<Sha256><Sha256>,
}

/******************/

/******************/

impl MerkleTree{
    /*
    pub fn new(leaves_hashed_sha256 : Vec<[u8; 32]>) -> Self{

        let merkle_tree = rs_merkle::RsMerkleTree<Sha256>::<Sha256>::from_leaves(&leaves_hashed_sha256);

        let merkle_root = merkle_tree.root().ok_or("couldn't get the merkle root").unwrap();

        Self{
            leaves_hashed_sha256,
            merkle_root,
            merkle_tree
        }
    }
    */

    pub fn getMerkleTree()->RsMerkleTree<Sha256>{
        RsMerkleTree::new()
    }

    //Method who allows to add leaf; todo!
    pub fn getMerkleProof(merkle_tree: RsMerkleTree<Sha256>, indices_to_prove: Vec<usize>) -> MerkleProof<Sha256>{
        let proof = merkle_tree.proof(&indices_to_prove);
        proof
        //proof
    }

    pub fn getMerkleRoot(merkle_tree: RsMerkleTree<Sha256>) -> [u8; 32]{
        merkle_tree.root().ok_or("couldn't get the merkle root").unwrap()
    }

    pub fn verifyMerkleProof(merkle_tree: RsMerkleTree<Sha256>, merkle_root: [u8; 32], leaves_hashed_sha256: Vec<[u8; 32]>, path: Path) -> bool{
        let merkle_proof = merkle_tree.proof(&path.getIndicesToProve());
        let proof_bytes = merkle_proof.to_bytes();
        let proof = MerkleProof::<Sha256>::try_from(proof_bytes).unwrap();
        proof.verify(merkle_root, &path.getIndicesToProve(), &path.getLeavesToProve(), leaves_hashed_sha256.len())
    }

    pub fn verifyMerkleProof_(merkle_tree: RsMerkleTree<Sha256>, merkle_root: [u8; 32], indices_to_prove: Vec<usize>, leaves_hashed_sha256: Vec<[u8; 32]>, leaves_to_prove: &[[u8; 32]]) -> bool{
        let merkle_proof = merkle_tree.proof(&indices_to_prove);
        let proof_bytes = merkle_proof.to_bytes();
        let proof = MerkleProof::<Sha256>::try_from(proof_bytes).unwrap();
        proof.verify(merkle_root, &indices_to_prove, leaves_to_prove, leaves_hashed_sha256.len())
    }

}