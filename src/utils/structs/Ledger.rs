use digest::typenum::Le;
use super::PourTransaction::TxPour;

pub struct Ledger{
    
}

impl Ledger{
    pub fn containsSn(sn: String)->bool{true}
    pub fn containsCm(cm: String)->bool{true}
    pub fn containsMerkleRoot(merkleRoot: [u8; 32])->bool{true}
    pub fn getTxPours()->Vec<TxPour>{Vec::<TxPour>::new()}
}