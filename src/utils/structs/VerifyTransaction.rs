use crate::utils::traits::transaction::Transaction;
use super::Ledger::Ledger;


pub fn verifyTransaction<Tx: Transaction>(_pp: String, tx: Tx, L: Ledger) -> bool{
    tx.verify()
}