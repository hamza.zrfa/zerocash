#![allow(warnings)]
use num_bigint::BigUint;
use rand::{SeedableRng, rngs::StdRng, Rng};
use rsa::{PaddingScheme, RsaPublicKey, RsaPrivateKey, PublicKey};

use crate::utils::{structs::{Ledger::Ledger, Address::{PublicAddress, PrivateAddress}, PourTransaction::TxPour, Coin::Coin}, hash_functions};

pub trait Transaction{
    fn verify(&self) -> bool;
    fn receive(_pp: String, addr_pk: PublicAddress, addr_sk: PrivateAddress) -> (Coin, Coin){

        let c_1 : Coin;
        let c_2 : Coin;

        //Get all tx_pour in the ledger
        let tx_pour_vec: Vec<TxPour> = Ledger::getTxPours();

        //For each i in {1, 2}
            //Computation of (v_i, rho_i, r_i, s_i):=D_enc(sk_enc, C_i)
            //We also have to ensure that the tuple obtained is well-formed. If it's not,
            //that means that the pour transaction was not sent for this public key
            todo!();
        

        for tx_pour in tx_pour_vec.iter(){
            let (v_1, rho_1, r_1, s_1) = decrypte_data_coin(addr_sk.sk_enc, tx_pour.getC1());
            let (v_2, rho_2, r_2, s_2) = decrypte_data_coin(addr_sk.sk_enc, tx_pour.getC2());

            //Ensure that cm_i-new equals COMM_{s_i}(v_i||COMM_{r_i}(a_{pk}||rho_i))
            let k_1 = Coin::computeCommitmentsK(addr_pk.a_pk.clone(), rho_1, r_1);
            let cm_1 = Coin::computeCommitmentsCm(v_1, k_1, s_1);

            let k_2 = Coin::computeCommitmentsK(addr_pk.a_pk.clone(), rho_2, r_2);
            let cm_2 = Coin::computeCommitmentsCm(v_2, k_2, s_2);

            assert_eq!(tx_pour.getCm1New(), cm_1);
            assert_eq!(tx_pour.getCm2New(), cm_2);

            //Ensure that sn_i:=PRF_{a_sk}-sn(rho_i) does not appears on L

            let rho_1_big_uint = BigUint::from_slice(&[rho_1]);
            let a_sk_big_uint = BigUint::from_slice(&[addr_sk.a_sk]);
        
            let rho_2_big_uint = BigUint::from_slice(&[rho_2]);
            let a_sk_big_uint = BigUint::from_slice(&[addr_sk.a_sk]);

                //We will commit to the sum of these two integers
            let sn_1 = hash_functions::getSha256Hash((rho_1_big_uint + a_sk_big_uint).to_bytes_le());
            let sn_2 = hash_functions::getSha256Hash((rho_2_big_uint + a_sk_big_uint).to_bytes_le());

            assert_eq!(Ledger::containsSn(sn_1), false);
            assert_eq!(Ledger::containsSn(sn_2), false);

            c_1 = Coin::new(addr_pk, v_1, rho_1, r_1, s_1, tx_pour.getCm1New());
            c_2 = Coin::new(addr_pk, v_2, rho_2, r_2, s_2, tx_pour.getCm2New());
        }

        (c_1, c_2)

    }
}

fn encrypt_data_coin(pk_enc: RsaPublicKey, data: (u32, u32, u32, u32)) -> Vec<u8>{
    let data_as_slice = tuple_to_bytes(data);
    let mut rng = rand::thread_rng();
    pk_enc.encrypt(&mut rng, PaddingScheme::new_pkcs1v15_encrypt(), &data_as_slice[..]).expect("failed to encrypt")
}

fn decrypte_data_coin(sk_enc: RsaPrivateKey, C: Vec<u8>) -> (u32, u32, u32, u32) {
    let padding = PaddingScheme::new_pkcs1v15_encrypt();
    let data_as_slice : [u8; 16] = sk_enc.decrypt(padding, &C).expect("failed to decrypt").try_into().unwrap();
    bytes_to_tuple(data_as_slice)
}

fn tuple_to_bytes(input: (u32, u32, u32, u32)) -> [u8; 16] {
    let (a, b, c, d) = input;
    let mut output = [0u8; 16];
    output[0..4].copy_from_slice(&a.to_le_bytes());
    output[4..8].copy_from_slice(&b.to_le_bytes());
    output[8..12].copy_from_slice(&c.to_le_bytes());
    output[12..16].copy_from_slice(&d.to_le_bytes());
    output
}

fn bytes_to_tuple(input: [u8; 16]) -> (u32, u32, u32, u32) {
    let a = u32::from_le_bytes(input[0..4].try_into().unwrap());
    let b = u32::from_le_bytes(input[4..8].try_into().unwrap());
    let c = u32::from_le_bytes(input[8..12].try_into().unwrap());
    let d = u32::from_le_bytes(input[12..16].try_into().unwrap());
    (a, b, c, d)
}

fn u128_to_bytes(value: u128) -> [u8; 32] {
    let mut arr = [0u8; 32];
    for (i, b) in value.to_le_bytes().iter().enumerate() {
        arr[i] = *b;
    }
    arr
}

fn bytes_to_u128(arr: [u8; 32]) -> u128 {
    let mut value = 0u128;
    for (i, b) in arr.iter().enumerate() {
        value |= (*b as u128) << (i * 8);
    }
    value
}


