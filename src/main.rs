extern crate web3;
extern crate num_bigint;
extern crate hex;
extern crate rs_merkle;

use hex::decode;
use num_bigint::BigUint;
use rand::rngs::StdRng;
use sha2::{Sha256, Digest};
use rsa::{PublicKey, RsaPrivateKey, RsaPublicKey, PaddingScheme};
use rand::{thread_rng, SeedableRng};
use utils::signature_fonctions::generate_keypair;
use utils::structs::Address::{PublicAddress, PrivateAddress};
use utils::structs::{Coin, VerifyTransaction};
use utils::structs::Encryption::Encryption;
use utils::structs::Path::Path;
use utils::structs::PourTransaction::TxPour;
use web3::futures::Future;
use web3::types::{TransactionRequest, U256};
use rs_merkle::{*, algorithms::Sha256 as OtherSha256};
use tokio::runtime::Runtime;

use crate::utils::hash_functions;

use crate::utils::structs::{MintTransaction, PourTransaction};

use crate::utils::structs::MerkleTree;

mod utils{
    pub mod signature_fonctions;
    pub mod hash_functions;
    pub mod structs{
        pub mod Address;
        pub mod Coin;
        pub mod Inputs;
        pub mod MintTransaction;
        pub mod Path;
        pub mod PourTransaction;
        pub mod Encryption;
        pub mod MerkleTree;
        pub mod Ledger;
        pub mod VerifyTransaction;
    }
    pub mod traits{
        pub mod transaction;
    }
}

//static merkle_tree: rs_merkle::MerkleTree<OtherSha256> = rs_merkle::MerkleTree::<OtherSha256>::new();

fn main (){

    /*
    //When the node starts, it have to get all the on chain data of the blockchain;
    //We suppose here that the merkle_tree has been retrieved properly
    */
    
    let merkle_tree: rs_merkle::MerkleTree<OtherSha256> = rs_merkle::MerkleTree::<OtherSha256>::new();

    //old_test();

    //pour_transaction_test();

    //mint_transaction_test();
    
}

fn mint_transaction_test(){
    let pp = String::new();
    let v = 0;
    println!("Generation of encryption keys: running");
    let secret_key_encryption = Encryption::generate_private_address_encryption();
    let public_key_encryption = Encryption::get_public_address_encryption(secret_key_encryption.clone());
    println!("Generation of encryption keys: done");
    let addr_pk = PublicAddress::new(0, public_key_encryption);
    let (coin, tx_mint) = MintTransaction::TxMint::mint(pp, v, addr_pk);

}

fn pour_transaction_test(){

    //Generation of signature keys
    let (public_key_signature, secret_key_signature) = generate_keypair();

    println!("Signature keys : done");

    //Generation of old encryption keys
    let secret_key_encryption_old_1 = Encryption::generate_private_address_encryption();
    let public_key_encryption_old_1 = Encryption::get_public_address_encryption(secret_key_encryption_old_1.clone());
    
    let secret_key_encryption_old_2 = Encryption::generate_private_address_encryption();
    let public_key_encryption_old_2 = Encryption::get_public_address_encryption(secret_key_encryption_old_2.clone());

    println!("Old encryption keys : done");

    //Generation of new encryption keys
    let secret_key_encryption_new_1 = Encryption::generate_private_address_encryption();
    let public_key_encryption_new_1 = Encryption::get_public_address_encryption(secret_key_encryption_new_1.clone());
    
    let secret_key_encryption_new_2 = Encryption::generate_private_address_encryption();
    let public_key_encryption_new_2 = Encryption::get_public_address_encryption(secret_key_encryption_new_2.clone());

    println!("New encryption keys : done");

    //Generation of old address 1
    let addr_pk_1_old = PublicAddress::new(0, public_key_encryption_old_1);
    let addr_sk_1_old = PrivateAddress::new(1, secret_key_encryption_old_1);

    println!("Old address 1 : done");

    //Generation of old address 2
    let addr_pk_2_old = PublicAddress::new(0, public_key_encryption_old_2);
    let addr_sk_2_old = PrivateAddress::new(1, secret_key_encryption_old_2);

    println!("Old address 2 : done");

    //Generation of new address 1
    let addr_pk_1_new = PublicAddress::new(0, public_key_encryption_new_1);
    let addr_sk_1_new = PrivateAddress::new(1, secret_key_encryption_new_1);

    println!("New address 1 : done");

    //Generation of new address 2
    let addr_pk_2_new = PublicAddress::new(0, public_key_encryption_new_2);
    let addr_sk_2_new = PrivateAddress::new(1, secret_key_encryption_new_2);

    println!("New address 2 : done");
    
    let _pp = 0;
    let rt : [u8; 32] = [12; 32];
    let c_1_old = Coin::Coin::new(addr_pk_1_old.clone(), 12, 12, 4242, 12, String::from("0x19739173c"));
    let c_2_old = Coin::Coin::new(addr_pk_2_old.clone(), 31, 21, 434, 93, String::from("0x93103e23a"));

    let path_1 = Path::new(Vec::new(), [[0;32]; 32]);
    let path_2 = Path::new(Vec::new(), [[0;32]; 32]);

    let v_1_new = 21;
    let v_2_new = 22;
    let v_pub = 0;

    let info = String::from("This is as test");

    println!("Start !");

    TxPour::pour(
        _pp,
        rt, 
        c_1_old, 
        c_2_old, 
        addr_sk_1_old,
        addr_sk_2_old,
        path_1,
        path_2,
        v_1_new,
        v_2_new,
        addr_pk_1_new,
        addr_pk_2_new,
        v_pub,
        info,
    );

    println!("End !");
}

fn old_test(){
    let data = "Hello world!";
    let hex_string = hash_functions::getSha256Hash(data);
    println!("hex hash: {}", hex_string);
    
    let mut rng = rand::thread_rng();
    let bits = 2048;
    let priv_key = RsaPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let pub_key = RsaPublicKey::from(&priv_key);

    // Encrypt
    let data = b"hello world";
    let enc_data = pub_key.encrypt(&mut rng, PaddingScheme::new_pkcs1v15_encrypt(), &data[..]).expect("failed to encrypt");
    assert_ne!(&data[..], &enc_data[..]);

    // Decrypt
    let dec_data = priv_key.decrypt(PaddingScheme::new_pkcs1v15_encrypt(), &enc_data).expect("failed to decrypt");
    assert_eq!(&data[..], &dec_data[..]);

    /////////////////////////////////////////////
    
    //
    //

    //transaction();

    //let mut PRF: StdRng = SeedableRng::from_seed();
}

fn transaction(){
    let mut rt = Runtime::new().unwrap();

    //let (_eloop, transport) = web3::transports::Http::new("http://localhost:8545").unwrap();
    let transport = web3::transports::Http::new("http://localhost:8545").unwrap();

    let web3 = web3::Web3::new(transport);

    //let accounts = web3.eth().accounts().wait().unwrap();
    let accounts = rt.block_on(web3.eth().accounts()).unwrap();

    let balance_before = rt.block_on(web3.eth().balance(accounts[1], None)).unwrap();

    let tx = TransactionRequest {
        from: accounts[0],
        to: Some(accounts[1]),
        gas: None,
        gas_price: None,
        value: Some(U256::from(10000)),
        data: None,
        nonce: None,
        condition: None,
        transaction_type: None,
        access_list: None,
        max_fee_per_gas: None,
        max_priority_fee_per_gas: None,
    };

    let tx_hash = rt.block_on(web3.eth().send_transaction(tx)).unwrap();

    let balance_after = rt.block_on(web3.eth().balance(accounts[1], None)).unwrap();

    println!("TX Hash: {:?}", tx_hash);
    println!("Balance before: {}", balance_before);
    println!("Balance after: {}", balance_after);
}